package com.example.simulterra2.indus.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.simulterra2.indus.Beans.ChatMessage;
import com.example.simulterra2.indus.Generics.BackGroundService;
import com.example.simulterra2.indus.Generics.Constants;
import com.example.simulterra2.indus.Generics.PrefManager;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.Utils.Utils;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();

    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private Button buttonSend;
    List<ChatMessage> chatMessagesArrayList = null;
    JSONArray chatMessages = null;
    Intent intent;
    private boolean side = false;
    public TextView txtNoData;
    PrefManager prefManager;
    String ReceiverMobNo = "";
    String ChatrromId = "";


    BroadcastReceiver mybrodcast;
    AVLoadingIndicatorView progresssChatActivity;
    Boolean addeddNewMsg = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        setContentView(R.layout.activity_chat);

        progresssChatActivity = (AVLoadingIndicatorView) findViewById(R.id.progresssChatActivity);

        chatMessagesArrayList = new ArrayList<>();
        txtNoData = (TextView) findViewById(R.id.txtNoData);
        txtNoData.setVisibility(View.GONE);
        prefManager = new PrefManager(getApplicationContext());
        Log.d("getUserID >", "" + prefManager.getUserID());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonSend = (Button) findViewById(R.id.buttonSend);

        listView = (ListView) findViewById(R.id.listView1);


        chatText = (EditText) findViewById(R.id.chatText);

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sendChatMessage();
            }
        });


        try {
            ChatrromId = getIntent().getStringExtra("ChatroomId");
            setCharromId(ChatrromId);
            MainApplication.SetActiveChatroom(ChatrromId);

            String UserName = getIntent().getStringExtra("UserName");
            Log.d("", "ChatrromId " + ChatrromId + UserName);

            setReceiverMobNo(UserName);
            getSupportActionBar().setTitle(UserName);
            if (ChatrromId.length() > 0) {
                new getMessages(ChatrromId).execute();
            } else {
                Utils.toastItShort(getApplicationContext(), "Something went wrong!");
            }

        } catch (Exception e) {

        }

    }


    /**
     * Set ChtrroomnId for this context
     *
     * @param chId
     */
    private void setCharromId(String chId) {

        this.ChatrromId = chId;
    }


    /**
     * send msg here
     */
    private void sendChatMessage() {
        if (chatText.getText().toString().trim().length() > 0) {
            ChatMessage cMsg = new ChatMessage();
            cMsg.setMsg(chatText.getText().toString().trim());
            cMsg.setFrom(prefManager.getMobileNo());
            //chatArrayAdapter.add(cMsg);
            // chatArrayAdapter.add(new ChatMessage(side, chatText.getText().toString()));
            // chatText.setText("");
            //side = !side;

            new AddMessage(ReceiverMobNo, chatText.getText().toString().trim()).execute();
        }
        // return true;
    }


    /**
     * Set setReceiverMobNo
     *
     * @param receiverMobNo
     */
    private void setReceiverMobNo(String receiverMobNo) {
        this.ReceiverMobNo = receiverMobNo;
    }


    /**
     * get Messages here
     */
    public class getMessages extends AsyncTask<Void, Void, Void> {

        String ChatoormId;

        public getMessages(String ChatoormId) {
            this.ChatoormId = ChatoormId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {

                progresssChatActivity.setVisibility(View.VISIBLE);
                Log.d(TAG, "in onPreExecute getMessages stop Receiver");
                stopService(new Intent(getApplicationContext(), BackGroundService.class));


                //threadflag = false;

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("chatRoomId", ChatoormId);
                json = jsonObject.toString();
                Log.d(TAG, " json " + json);

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.GetAllMessages);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);

                //Received response:  {"respCode":"00","respDesc":"User Created","userId":"7"}

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                chatMessages = jObject.getJSONArray("chats");
                // looping through All Contacts
                for (int i = 0; i < chatMessages.length(); i++) {
                    JSONObject c = chatMessages.getJSONObject(i);

                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.setFrom(c.getString("srcUserId"));
                    chatMessage.setMsg(c.getString("msg"));
                    chatMessage.setId(c.getString("msgId"));

                    Log.d(TAG, "chatMessage  " + c.getString("msg"));


                    if (checkExistance(chatMessage)) {
                        Log.d(TAG, "chatMessage  " + c.getString("msg") + " is already exist");
                    } else {

                        Log.d(TAG, "chatMessage  " + c.getString("msg") + " Not Exixt.. adding Now");
                        chatMessagesArrayList.add(chatMessage);
                    }

                  /*  for (i = 0; is < is; is++) {


                        ChatMessage cs = chatMessagesArrayList.get(is);
                        cs.equals(chatMessage);

                        //   chatMessagesArrayList.get(is).getId.(c.getString("msgId"));
                    }

                    if (chatMessagesArrayList.contains(chatMessage)) {
                        Log.d(TAG, "chatMessage  " + c.getString("msg") + " is already exist");
                    } else {
                        chatMessagesArrayList.add(chatMessage);
                    }*/


                }

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {

            }
            progresssChatActivity.setVisibility(View.GONE);
            Log.d(TAG, "in onPostExecute getMessages start Receiver");
            startService(new Intent(ChatActivity.this, BackGroundService.class));
            if (chatMessagesArrayList.size() > 0) {
                listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);                // listView.setAdapter(chatArrayAdapter);

                chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage, chatMessagesArrayList);
                listView.setAdapter(chatArrayAdapter);
                txtNoData.setVisibility(View.GONE);
            } else {
                txtNoData.setVisibility(View.VISIBLE);
                // Utils.toastItShort(ChatActivity.this, "No Messsages found");
            }


        }
    }


    private boolean checkExistance(ChatMessage chatMessage) {
        int is = chatMessagesArrayList.size();
        for (is = 0; is < is; is++) {
            ChatMessage cs = chatMessagesArrayList.get(is);
            Log.d(TAG, "in chatMessagesArrayList > " + cs.getMsg());
            Log.d(TAG, "in chatMessagesArrayList < " + chatMessage.getMsg());

            if (cs.equals(chatMessage)) {
                return true;
            } else {
                return false;
            }
            //ChatMessage cs = chatMessagesArrayList.get(is);
            // cs.getId().equals(chatMessage);

        }
        return false;


    }



    /**
     * Send message Here
     */
    public class AddMessage extends AsyncTask<Void, Void, Void> {

        String mob;
        String msg;


        public AddMessage(String mob, String msg) {
            this.mob = mob;
            this.msg = msg;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                chatText.setText("");
                Log.d(TAG, "in onPreExecute AddMessage   stop Receiver");
                stopService(new Intent(getApplicationContext(), BackGroundService.class));

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("srcMobNo", prefManager.getMobileNo());
                jsonObject.accumulate("destMobNo", mob);
                jsonObject.accumulate("msg", msg);
                json = jsonObject.toString();
                Log.d("json", json);
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.AddMessageService);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);

                //Received response:  {"respCode":"00","respDesc":"User Created","userId":"7"}

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                String respDesc = jObject.getString("respDesc");

                Log.d("respDesc", respDesc);

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {

                Log.d(TAG, "in onPostExecute AddMessage registerReceiver");
                startService(new Intent(ChatActivity.this, BackGroundService.class));

           /*     ChatMessage cMsg = new ChatMessage();
                cMsg.setMsg(chatText.getText().toString().trim());
                cMsg.setFrom(prefManager.getUserID());

                chatArrayAdapter.add(cMsg);
                chatArrayAdapter.notifyDataSetChanged();*/
                // chatText.setText("");
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                MainApplication.SetActiveChatroom(null);
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        MainApplication.SetActiveChatroom(null);
        ChatActivity.this.finish();

    }

    public class ChatArrayAdapter extends ArrayAdapter<ChatMessage> {

        private TextView chatText;
        private List<ChatMessage> chatMessageList = new ArrayList<ChatMessage>();
        private LinearLayout singleMessageContainer;
        Context c;
        int text;
        //PrefManager prefManager;

        @Override
        public void add(ChatMessage object) {
            chatMessageList.add(object);
            super.add(object);
        }

        public ChatArrayAdapter(Context context, int textViewResourceId, List<ChatMessage> list) {
            //super();
            super(context, textViewResourceId);
            this.c = context;
            this.text = textViewResourceId;
            this.chatMessageList = list;
        }

        public int getCount() {
            return this.chatMessageList.size();
        }

        public ChatMessage getItem(int index) {
            return this.chatMessageList.get(index);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.activity_chat_singlemessage, parent, false);
            }
            singleMessageContainer = (LinearLayout) row.findViewById(R.id.singleMessageContainer);
            ChatMessage chatMessageObj = getItem(position);
            chatText = (TextView) row.findViewById(R.id.singleMessage);
            chatText.setText(chatMessageObj.getMsg());
            //chatText.setBackgroundResource(chatMessageObj.left ? R.drawable.bubble_a : R.drawable.bubble_b);
            //singleMessageContainer.setGravity(chatMessageObj.left ? Gravity.LEFT : Gravity.RIGHT);

            //   prefManager = new PrefManager(getContext());
            Log.d("getUserID", "" + prefManager.getUserID());
            chatText.setBackgroundResource(chatMessageObj.getFrom().equals(prefManager.getUserID()) ? R.drawable.bubble_b : R.drawable.bubble_a);
            singleMessageContainer.setGravity(chatMessageObj.getFrom().equals(prefManager.getUserID()) ? Gravity.RIGHT : Gravity.LEFT);
            return row;
        }

        public Bitmap decodeToBitmap(byte[] decodedByte) {
            return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
        }

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


        //new GetInformation().execute();

        IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");
        mybrodcast = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                stopService(new Intent(context, BackGroundService.class));
                //  new GetInformationBackgroung().execute();
                new getMessagesInBackground(ChatrromId).execute();

                //new GetUpdatedmsg().execute();
                //log our message value
                Log.i("Broadcast.........", "Msg Recevied..............");

            }
        };

        registerReceiver(mybrodcast, intentFilter);
        Log.d(TAG, "in onResume registerReceiver");

    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG, "in pause");
        stopService(new Intent(this, BackGroundService.class));
        unregisterReceiver(mybrodcast);
    }


    public class getMessagesInBackground extends AsyncTask<Void, Void, Void> {

        String ChatoormId;

        public getMessagesInBackground(String ChatoormId) {
            this.ChatoormId = ChatoormId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {


                Log.d(TAG, "in onPreExecute getMessagesInBackground stop Receiver");
                stopService(new Intent(getApplicationContext(), BackGroundService.class));


                //threadflag = false;

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("chatRoomId", ChatoormId);
                json = jsonObject.toString();
                Log.d(TAG, " json " + json);

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.GetAllMessages);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);

                //Received response:  {"respCode":"00","respDesc":"User Created","userId":"7"}

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                chatMessages = jObject.getJSONArray("chats");
                // looping through All Contacts
                for (int i = 0; i < chatMessages.length(); i++) {
                    JSONObject c = chatMessages.getJSONObject(i);

                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.setFrom(c.getString("srcUserId"));
                    chatMessage.setMsg(c.getString("msg"));
                    chatMessage.setId(c.getString("msgId"));

                    Log.d(TAG, "chatMessage  " + c.getString("msg"));

                    if (chatMessagesArrayList.contains(chatMessage)) {
                        Log.d(TAG, "chatMessage  " + c.getString("msg") + " is already exist");
                        addeddNewMsg = false;
                    } else {
                        Log.d(TAG, "chatMessage  " + c.getString("msg") + " Not Exixt.. adding Now");
                        chatMessagesArrayList.add(chatMessage);
                        addeddNewMsg = true;
                    }

                    /*if (checkExistance(chatMessage)) {
                        // addeddNewMsg=false;
                        Log.d(TAG, "chatMessage  " + c.getString("msg") + " is already exist");
                    } else {

                        addeddNewMsg = true;
                        Log.d(TAG, "chatMessage  " + c.getString("msg") + " Not Exixt.. adding Now");
                        chatMessagesArrayList.add(chatMessage);
                    }*/


                }

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {

            }
            Log.d(TAG, "in onPostExecute getMessagesInBackground start Receiver");
            startService(new Intent(ChatActivity.this, BackGroundService.class));
            if (chatMessagesArrayList.size() > 0) {
                // listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                // listView.setAdapter(chatArrayAdapter);

                if (addeddNewMsg) {
                    listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                    Log.d(TAG, "Added msg " + chatMessagesArrayList.size());
                    addeddNewMsg = false;
                    //chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage, chatMessagesArrayList);
                    //listView.setAdapter(chatArrayAdapter);

                    chatArrayAdapter.notifyDataSetChanged();

                } else {
                    listView.setOverScrollMode(View.OVER_SCROLL_NEVER);
                    Log.d(TAG, "Nothing New to Add " + chatMessagesArrayList.size());
                }
                txtNoData.setVisibility(View.GONE);

            } else {
                txtNoData.setVisibility(View.VISIBLE);
                // Utils.toastItShort(ChatActivity.this, "No Messsages found");
            }

            /*listView.post(new Runnable() {
                public void run() {
                    // chatlist.setSelection(messages.size());
                    listView.smoothScrollToPosition(chatMessagesArrayList.size());
                }
            });*/


          /*  listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.setSelection(listView.getCount() - 1);
                }
            });
*/

        }
    }


}