package com.example.simulterra2.indus.Beans;

/**
 * Created by Ranajit on 14-07-2016.
 */
public class ChatMessage {

    private String from = "";
    private String msg = "";
    private long sentOn = 0;
    private String id = "";


    public boolean equals(Object obj) {

        if (!(obj instanceof ChatMessage)) {
            return false; //objects cant be equal
        }
        ChatMessage anotherMessage = (ChatMessage) obj;


        return this.id.equals(anotherMessage.id);

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    private boolean unread = true;

    public ChatMessage() {
    }

    public ChatMessage(String from, String msg, long sentOn) {
        this.from = from;
        this.msg = msg;
        this.sentOn = sentOn;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getSentOn() {
        return sentOn;
    }

    public void setSentOn(long sentOn) {
        this.sentOn = sentOn;
    }


    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }


}
