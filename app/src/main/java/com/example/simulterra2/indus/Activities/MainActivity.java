package com.example.simulterra2.indus.Activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.simulterra2.indus.Adapters.ViewPagerAdapter;
import com.example.simulterra2.indus.Fragments.GroupsFragment;
import com.example.simulterra2.indus.Fragments.MsgFragment;
import com.example.simulterra2.indus.Fragments.StarredFragment;
import com.example.simulterra2.indus.R;

//import android.support.v4.app.Fragment;

public class MainActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();

    private DrawerLayout mDrawerLayout;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private TextView txtGroups, txtStarred, txtMsgs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);

       /* mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                Toast.makeText(MainActivity.this, menuItem.getTitle(), Toast.LENGTH_LONG).show();
                return true;
            }
        });*/


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //  viewPager = (ViewPager) findViewById(R.id.viewpager);
        //   setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        ///tabLayout.setupWithViewPager(viewPager);
        // setupTabIcons();

        txtStarred = (TextView) findViewById(R.id.txtStarred);
        txtGroups = (TextView) findViewById(R.id.txtGroups);
        txtMsgs = (TextView) findViewById(R.id.txtMsgs);


        Fragment newFragment = new MsgFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.Container, newFragment);
        transaction.commit();

        txtStarred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                txtStarred.setTextColor(getResources().getColor(R.color.white));
                txtGroups.setTextColor(getResources().getColor(R.color.darker_gray));
                txtMsgs.setTextColor(getResources().getColor(R.color.darker_gray));


                Fragment newFragment = new StarredFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.Container, newFragment);
                transaction.commit();

            }
        });
        txtGroups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                txtGroups.setTextColor(getResources().getColor(R.color.white));
                txtMsgs.setTextColor(getResources().getColor(R.color.darker_gray));
                txtStarred.setTextColor(getResources().getColor(R.color.darker_gray));


                Fragment newFragment = new GroupsFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.Container, newFragment);
                transaction.commit();

            }
        });

        txtMsgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMsgs.setTextColor(getResources().getColor(R.color.white));
                txtGroups.setTextColor(getResources().getColor(R.color.darker_gray));
                txtStarred.setTextColor(getResources().getColor(R.color.darker_gray));


                Fragment newFragment = new MsgFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.Container, newFragment);
                transaction.commit();
            }
        });

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            try {

                String receivedMessage = extras.getString("Message");
                String chatroomID = extras.getString("chatroomID");
                String MobNo = extras.getString("MobNo");
                System.out.println("receivedMessage  : " + receivedMessage);
                //startActivity(new Intent());
                startActivity(new Intent(getApplicationContext(), ChatActivity.class).putExtra("ChatroomId", chatroomID).putExtra("UserName", MobNo));

                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(Integer.parseInt(chatroomID));

            } catch (Exception e) {

            }

            //tv.setText("Data Sent from Clicking Notification nData 1 : " + data1 + "nData 2 : " + data2);
        }

//test tests
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
              //  mDrawerLayout.openDrawer(GravityCompat.START);
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("STARRED");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.starred, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("DIRECT MSG");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.direct, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("GROUPS");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.groups, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        //adapter.addFrag(new StarredFragment(), "STARRED");
        //  adapter.addFrag(new MsgFragment(), "DIRECT MSG");
        // adapter.addFrag(new GroupsFragment(), "GROUPS");
        viewPager.setAdapter(adapter);
    }


}
