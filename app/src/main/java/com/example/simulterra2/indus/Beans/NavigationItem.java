package com.example.simulterra2.indus.Beans;

public class NavigationItem {

	private String title;

	public NavigationItem(String title) {
		this.title = title;

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}