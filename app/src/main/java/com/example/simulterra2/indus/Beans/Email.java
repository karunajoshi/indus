package com.example.simulterra2.indus.Beans;

/**
 * Created by admin on 17-08-2016.
 */
public class Email {

    private String from = "";
    private String to = "";
    private String body = "";
    private String subj = "";

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubj() {
        return subj;
    }

    public void setSubj(String subj) {
        this.subj = subj;
    }
}
