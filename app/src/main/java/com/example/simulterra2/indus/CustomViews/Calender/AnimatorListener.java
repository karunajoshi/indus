package com.example.simulterra2.indus.CustomViews.Calender;


import android.animation.Animator;

public abstract class AnimatorListener implements Animator.AnimatorListener{

    @Override
    public void onAnimationStart(Animator animation) {
    }

    @Override
    public void onAnimationEnd(Animator animation) {

    }

    @Override
    public void onAnimationCancel(Animator animation) {
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }

}
