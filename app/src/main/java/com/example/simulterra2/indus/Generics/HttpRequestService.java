package com.example.simulterra2.indus.Generics;


import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Ranajit on 10-07-2016.
 */
public final class HttpRequestService extends IntentService {

    public static final String TYPE_POST = "POST";
    public static final String TYPE_GET = "TYPE_GET";
    public static final String TYPE_UPLOAD = "UPLOAD";
    public static final String FILE_PATH = "filePath";
    private final String TAG = this.getClass().getSimpleName();

    /**
     * Mandatory constructor
     */
    public HttpRequestService(String name) {

        super(name);
    }

    /**
     * Zero arg constructor
     */
    public HttpRequestService() {
        super("HttpRequestService");
    }

    /**
     * Handle Intent to send POST or TYPE_GET request
     *
     * @param intent
     */
    @Override
    public void onHandleIntent(Intent intent) {
        String requestType = intent.getStringExtra(Constants.HTTP_REQUEST_TYPE);

        if (requestType.equalsIgnoreCase(TYPE_POST)) {
            processPOSTRequest(intent);
        } else if (requestType.equalsIgnoreCase(TYPE_GET)) {
            processGETRequest(intent);
        } else if (requestType.equalsIgnoreCase(TYPE_UPLOAD)) {
            processUploadRequest(intent);
        }
        stopSelf();
    }

    /**
     * Handle a POST request
     *
     * @param intent
     */
    private void processPOSTRequest(Intent intent) {
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        String result = "";

        String urlString = intent.getStringExtra(Constants.HTTP_REQUEST_URL);
        String intentFilter = intent.getStringExtra(Constants.HTTP_ACTION_FILTER);

        Bundle params = intent.getExtras();
        params.remove(Constants.HTTP_REQUEST_URL);
        params.remove(Constants.HTTP_ACTION_FILTER);
        params.remove(Constants.HTTP_REQUEST_TYPE);

        Log.d(TAG, "HTTP PARAMS = " + params.toString());

        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.connect();

            //Create JSONObject here
            JSONObject jsonParam = new JSONObject();
            for (String key : params.keySet()) {
                if (key.equalsIgnoreCase("jsonObject"))
                {
                    JSONObject json = new JSONObject(params.getString(key));
                    Iterator<String> keys = json.keys();
                    if (keys.hasNext())
                    {
                        String keyStr = keys.next();
                        JSONObject keyValue = new JSONObject(json.getString(keyStr));
                        jsonParam.put(keyStr, keyValue);
                        Log.d(TAG, "key:" + keyStr + ", keyval:" + keyValue);
                    }
                }
                else
                {
                    jsonParam.put(key, params.get(key));
                }
            }

            Log.d(TAG, "JSON HTTP PARAMS = " + jsonParam.toString());
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonParam.toString());
            out.close();

            int HttpResult = urlConnection.getResponseCode();

            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                Log.d(TAG, "Received from server: " + sb.toString());

                result = sb.toString();
            } else {
                Log.d(TAG, "Server Response: " + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        // Create new Intent with URI object
        Intent localIntent = new Intent(intentFilter);
        localIntent.putExtra(Constants.HTTP_RESPONSE_DATA, result);

        // Broadcast intent to receivers in this app only.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    public byte[] getImage(Context ctx, String localPath) {
        try {
            byte[] data = new byte[(int) new File(localPath).length()];
            new FileInputStream(localPath).read(data);
            //String encodedImage = Base64.encodeToString(data, Base64.DEFAULT);
            return data;
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error reading local file for image upload.", e);
        } catch (IOException e) {
            throw new RuntimeException("Error reading local file for image upload.", e);
        }
    }

    /**
     * Handle a upload request
     *
     * @param intent
     */
    private void processUploadRequest(Intent intent) {
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        String result = "";

        String urlString = intent.getStringExtra(Constants.HTTP_REQUEST_URL);
        String intentFilter = intent.getStringExtra(Constants.HTTP_ACTION_FILTER);

        Bundle params = intent.getExtras();
        params.remove(Constants.HTTP_REQUEST_URL);
        params.remove(Constants.HTTP_ACTION_FILTER);
        params.remove(Constants.HTTP_REQUEST_TYPE);

        Log.d(TAG, "HTTP PARAMS = " + params.toString());

        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("PUT");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "image/jpeg");
            urlConnection.connect();

            //Create JSONObject here
//            JSONObject jsonParam = new JSONObject();
//            for (String key : params.keySet()) {
//                jsonParam.put(key, params.get(key));
//            }
            byte[] file = getImage(this, (String) params.get(FILE_PATH));

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(file);
            outputStream.close();

            int HttpResult = urlConnection.getResponseCode();

            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                Log.d(TAG, "Received from server: " + sb.toString());

                result = sb.toString();
            } else {
                Log.d(TAG, "Server Response: " + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        // Create new Intent with URI object
        Intent localIntent = new Intent(intentFilter);
        localIntent.putExtra(Constants.HTTP_RESPONSE_DATA, result);

        // Broadcast intent to receivers in this app only.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    /**
     * Handle a TYPE_GET request
     *
     * @param intent
     */
    private void processGETRequest(Intent intent) {
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        String result = "";

        String urlString = intent.getStringExtra(Constants.HTTP_REQUEST_URL);
        String intentFilter = intent.getStringExtra(Constants.HTTP_ACTION_FILTER);

        Bundle params = intent.getExtras();
        params.remove(Constants.HTTP_REQUEST_URL);
        params.remove(Constants.HTTP_ACTION_FILTER);
        params.remove(Constants.HTTP_REQUEST_TYPE);

        String responseString = null;
        try {
            Uri.Builder builtUri = Uri.parse(urlString).buildUpon();

            for (String key : params.keySet()) {
                builtUri.appendQueryParameter(key, (String) params.get(key));
            }

            Log.d(TAG, "URL = " + builtUri.build().toString());

            URL url = new URL(builtUri.build().toString());

            urlConnection = (HttpURLConnection) url.openConnection();

            int HttpResult = urlConnection.getResponseCode();

            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                Log.d(TAG, "Received from server: " + sb.toString());

                result = sb.toString();
            } else {
                Log.d(TAG, "Server Response: " + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        // Create new Intent with URI object
        Intent localIntent = new Intent(intentFilter);
        localIntent.putExtra(Constants.HTTP_RESPONSE_DATA, result);

        // Broadcast intent to receivers in this app only.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }


    public static void invoke(Context ctx, String url, String type, String filter, Bundle extras, BroadcastReceiver receiver) {
        final Intent httpRequestServiceIntent = new Intent(ctx, HttpRequestService.class);
        httpRequestServiceIntent.putExtra(Constants.HTTP_REQUEST_TYPE, type);
        httpRequestServiceIntent.putExtra(Constants.HTTP_REQUEST_URL, url);
        httpRequestServiceIntent.putExtra(Constants.HTTP_ACTION_FILTER, filter);
        httpRequestServiceIntent.putExtras(extras);

        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(filter));
        ctx.startService(httpRequestServiceIntent);
    }
}