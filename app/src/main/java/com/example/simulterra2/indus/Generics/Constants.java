package com.example.simulterra2.indus.Generics;

/**
 * Created by Ranajit on 10-07-2016.
 */
public class Constants {


    // HTTP Call Constants
    public static final String HTTP_RESPONSE_DATA = "httpData";
    public static final String HTTP_REQUEST_TYPE = "requestType";
    public static final String HTTP_REQUEST_URL = "urlString";
    public static final String HTTP_ACTION_FILTER = "actionFilter";


    /**************************************************************************
     * GCM Specific Constants
     *************************************************************************/
    public static final String SENDER_ID = "994549929333";

    /**************************************************************************
     * API Calls
     *************************************************************************/

    public static final String SERVER_URL = "http://205.147.97.129:8080/";
    public static final String CreateUserService = SERVER_URL + "GCM_Chat/CreateUserService";
    public static final String CreateChatRoomService = SERVER_URL + "GCM_Chat/CreateChatRoomService";
    public static final String GetAllChatRoomService = SERVER_URL + "GCM_Chat/GetAllChatRoomService";
    public static final String AddMessageService = SERVER_URL + "GCM_Chat/AddMessageService";
    public static final String GetAllMessages = SERVER_URL + "GCM_Chat/GetAllMessages";
    public static final String GetUserFromPhoneBookService = SERVER_URL + "GCM_Chat/GetUserFromPhoneBookService";
    public static final String GetEmailListService = SERVER_URL + "GCM_Chat/GetEmailListService";


    /**************************************************************************
     * Message Constants
     *************************************************************************/
    public static final String TYPE_GROUP = "G";
    public static final String TYPE_SINGLE = "S";
    public static final String UNREAD = "1";
    public static final String READ = "0";
    public static final String SEEN = "1";
    public static final String UNSEEN = "0";

}
