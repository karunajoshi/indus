package com.example.simulterra2.indus.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.simulterra2.indus.Beans.ChatRoom;
import com.example.simulterra2.indus.Beans.Files;
import com.example.simulterra2.indus.CustomViews.multipleImagePicker.utils.ImageInternalFetcher;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.Utils.Utils;
import com.example.simulterra2.indus.ViewHolders.ChatRoomViewHolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import android.support.v7.internal.view.ContextThemeWrapper;
//import android.support.v7.view.ContextThemeWrapper;

public class DriveActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();
    private File root;
    private ArrayList<File> fileList = new ArrayList<File>();
    private RecyclerView recyclerview;
    DriveAdapter rcAdapter;

    ArrayList<Files> FileList = null;
    String strFolderName;
    File newfolderPath;
    private static final int PICKFILE_RESULT_CODE = 1;

    public ImageInternalFetcher mImageFetcher;
    RelativeLayout layMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive);
        getSupportActionBar().setTitle("Drive");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layMain = (RelativeLayout) findViewById(R.id.layMain);
        root = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath());
        mImageFetcher = new ImageInternalFetcher(DriveActivity.this, 500);
        root = new File(android.os.Environment.getExternalStorageDirectory().getPath() + "/Pictures/");
        FileList = new ArrayList<>();

        recyclerview = (RecyclerView) findViewById(R.id.recyclerViewEmail);
        recyclerview.setLayoutManager(new GridLayoutManager(DriveActivity.this, 2));


    }


    @Override
    protected void onResume() {
        super.onResume();
        setDataToAdapter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * set data to the adapter
     */
    public void setDataToAdapter() {
        FileList.clear();
        FileList = new ArrayList<>();
        getfile(root);

        rcAdapter = new DriveAdapter(getApplicationContext(), FileList);

        if (FileList.size() > 0) {
            // txtNoData.setVisibility(View.GONE);
            recyclerview.setAdapter(rcAdapter);
        } else {
            // txtNoData.setVisibility(View.VISIBLE);
            Utils.toastItShort(getApplicationContext(), "No Files Found");
        }
    }


    /**
     * get all the file from the directory
     *
     * @param dir
     */
    public void getfile(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {


            for (int i = 0; i < listFile.length; i++) {

                Files files = new Files();

                if (listFile[i].isDirectory()) {
                    fileList.add(listFile[i]);
                    // getfile(listFile[i]);
                    files.setFilename(listFile[i].getName());
                    files.setFileType("Folder");
                    files.setFileUri("null");
                    FileList.add(files);


                } else if (listFile[i].getName().endsWith(".png")
                        || listFile[i].getName().endsWith(".jpg")
                        || listFile[i].getName().endsWith(".jpeg")
                        || listFile[i].getName().endsWith(".gif"))

                {
                    //fileList.add(listFile[i]);
                    files.setFilename(listFile[i].getName());
                    files.setFileUri(listFile[i].getAbsolutePath());
                    files.setFileType("Img");
                    FileList.add(files);

                } else {
                    files.setFilename(listFile[i].getName());
                    files.setFileUri("no");
                    files.setFileType("Doc");
                    FileList.add(files);
                }
                //}

            }
        }
        // return fileList;
    }


    /**
     * set adapter to drive data
     */
    public class DriveAdapter extends RecyclerView.Adapter<ChatRoomViewHolder> {


        private List<Files> itemList;
        private Context context;
        HashMap<String, ChatRoom> mSelectedProspects = new HashMap<>();
        ChatRoom p;
        //  public final GroupsFragment.OnItemClickListener listener;


        public DriveAdapter(Context context, List<Files> itemList) {
            this.itemList = itemList;
            this.context = context;
            //  this.listener = listener;
        }

        @Override
        public ChatRoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_drive, null);//list_item_responded_users
            ChatRoomViewHolder rcv = new ChatRoomViewHolder(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(ChatRoomViewHolder holder, int position) {

            //  holder.bind(itemList.get(position), listener);
            Log.d("", "itemList " + itemList.get(position).getFilename());

            try {

                if (itemList.get(position).getFileType().equals("Img")) {
                    holder.imgFileType.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    // holder.imgFileType.setImageURI(Uri.parse(itemList.get(position).getFileUri()));

                    mImageFetcher.loadImage(Uri.parse(itemList.get(position).getFileUri()), holder.imgFileType);

                } else if (itemList.get(position).getFileType().equals("Folder")) {

                    holder.imgFileType.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    holder.imgFileType.setImageResource(R.drawable.folders);
                    // mImageFetcher.loadImage(R.drawable.folders,  holder.imgFileType);

                } else {
                    holder.imgFileType.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    //mImageFetcher.loadImage(R.drawable.doc_file,  holder.imgFileType);
                    holder.imgFileType.setImageResource(R.drawable.doc_file);
                }

            } catch (Exception e) {

            }


            holder.txtFileName.setText(itemList.get(position).getFilename());


        }

        @Override
        public int getItemCount() {
            return this.itemList.size();
        }


    }


    public void onFolderClick(View v) {
        showCreateFolderDialog(getApplicationContext());

    }


    public void onUploadClick(View v) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICKFILE_RESULT_CODE);
    }


    public void onScanClick(View v) {
        startActivity(new Intent(DriveActivity.this, ActivityComingSoon.class));
    }


    /**
     * show dialog for group name
     *
     * @param context
     */
    public void showCreateFolderDialog(Context context) {
        //    ContextThemeWrapper ctw = new ContextThemeWrapper(getApplicationContext(), R.style.MyAlertDialogStyle);
        //   AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctw);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_with_ediitext, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        dialogBuilder.setTitle("                 Create Folder                  ");
        // dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                strFolderName = new String();

                if (edt.getText().toString().trim().length() > 0) {

                    strFolderName = edt.getText().toString();

                    newfolderPath = new File(android.os.Environment.getExternalStorageDirectory().getPath() + "/Pictures/" + strFolderName + "/");

                    if (newfolderPath.exists()) {
                        Utils.toastItShort(getApplicationContext(), strFolderName + " is already exist!");
                    } else {
                        newfolderPath.mkdir();
                        setDataToAdapter();
                    }

                }

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == RESULT_OK) {

                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    String path = myFile.getAbsolutePath();
                    String displayName = null;

                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = getContentResolver().query(uri, null, null, null, null);

                            String filePath = "";
                            String[] column = {MediaStore.MediaColumns.DATA};

                            int columnIndex = cursor.getColumnIndex(column[0]);

                            if (cursor.moveToFirst()) {

                                filePath = cursor.getString(columnIndex);
                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                Log.d(TAG, " displayName : " + filePath + " name  " + displayName);

                                CopyFile(filePath, displayName);

                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (uriString.startsWith("file://")) {
                        displayName = myFile.getName();
                        CopyFile(myFile.getAbsolutePath(), displayName);

                        Log.d(TAG, " displayName : " + displayName + " " + uri.toString());
                    }


                }
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    /**
     * copy file
     *
     * @param filePath
     * @param fileName
     */
    private void CopyFile(String filePath, String fileName) {
        File oldfile = new File(filePath);

        try {
            if (oldfile.exists()) {

                File targetLocation = new File(android.os.Environment.getExternalStorageDirectory().getPath() + "/Pictures/" + fileName);

                InputStream in = new FileInputStream(oldfile);
                OutputStream out = new FileOutputStream(targetLocation);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                in.close();
                out.close();

                Utils.showSnack(layMain, "File Uploaded");
                Log.v(TAG, "Copy file successful.");

            } else {

                Utils.showSnack(layMain, "File Not Uploaded! ");
                Log.v(TAG, "Copy file failed. Source file missing.");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
