package com.example.simulterra2.indus.Generics;

import android.content.SharedPreferences;
import android.content.Context;
import android.content.SharedPreferences.Editor;

/**
 * Created by Ranajit on 10-07-2016.
 */
public class PrefManager {


    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "Indus";


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    //Example

    public void setSampleString(String SampleString) {
        editor.putString("SampleString", SampleString);
        editor.commit();
    }

    public String getSampleString() {
        return pref.getString("SampleString", null);
    }


    public void setEmailID(String EmailID) {
        editor.putString("EmailID", EmailID);
        editor.commit();
    }

    public String getEmailID() {
        return pref.getString("EmailID", null);
    }


    public void setMobileNo(String MobileNo) {
        editor.putString("MobileNo", MobileNo);
        editor.commit();
    }

    public String getMobileNo() {
        return pref.getString("MobileNo", null);
    }


    public void setUserID(String UserID) {
        editor.putString("UserID", UserID);
        editor.commit();
    }

    public String getUserID() {
        return pref.getString("UserID", null);
    }


    public void setDeviceToken(String DeviceToken) {
        editor.putString("DeviceToken", DeviceToken);
        editor.commit();
    }

    public String getDeviceToken() {
        return pref.getString("DeviceToken", null);
    }


}
