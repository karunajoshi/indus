package com.example.simulterra2.indus.Adapters;

/**
 * Created by Ranajit on 14-07-2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simulterra2.indus.Beans.ChatRoom;
import com.example.simulterra2.indus.Fragments.GroupsFragment;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.ViewHolders.ChatRoomViewHolder;

import java.util.HashMap;
import java.util.List;

/**
 * set Adapter for Responded chat users
 */
public class ChatUsersAdapter extends RecyclerView.Adapter<ChatRoomViewHolder> {


    private List<ChatRoom> itemList;
    private Context context;
    HashMap<String, ChatRoom> mSelectedProspects = new HashMap<>();
    ChatRoom p;
    public final GroupsFragment.OnItemClickListener listener;


    public ChatUsersAdapter(Context context, List<ChatRoom> itemList, GroupsFragment.OnItemClickListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ChatRoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_direct_msg, null);//list_item_responded_users
        ChatRoomViewHolder rcv = new ChatRoomViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ChatRoomViewHolder holder, int position) {

        holder.bind(itemList.get(position), listener);
        Log.d("", "itemList " + itemList.get(position).getType());
        holder.txtName.setText(itemList.get(position).getMemberId());
        holder.txtMsg.setText(itemList.get(position).getLastMsg());



    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


}