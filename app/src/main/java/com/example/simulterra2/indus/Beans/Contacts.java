package com.example.simulterra2.indus.Beans;

/**
 * Created by Ranajit on 14-07-2016.
 */
public class Contacts {
    public String id;
    public String name;
    //	public String email;
    public String number;

    public Contacts(String id, String name) {
        this.id = id;
        this.name = name;
    }


    public Contacts(String id, String name, String number) {
        this.id = id;
        this.name = name;
//		this.email = email;
        this.number = number;
    }

    @Override
    public String toString() {
//		return "name: " + name + "mobile: " + number + "email: " + email;
        return name + "," + number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
