package com.example.simulterra2.indus.Generics.GCM;

/**
 * Created by Ranajit on 10-07-2016.
 */


public class GCMPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String REGISTRATION_TOKEN = "registrationToken";

}