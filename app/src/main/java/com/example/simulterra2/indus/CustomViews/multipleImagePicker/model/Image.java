package com.example.simulterra2.indus.CustomViews.multipleImagePicker.model;

import android.net.Uri;


public class Image {

    public Uri mUri;
    public int mOrientation;

    public Image(Uri uri, int orientation){
        mUri = uri;
        mOrientation = orientation;
    }

}
