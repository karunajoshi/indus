package com.example.simulterra2.indus.Generics;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.simulterra2.indus.Activities.MainActivity;
import com.example.simulterra2.indus.Activities.MainApplication;
import com.example.simulterra2.indus.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 17-07-2016.
 */
public  class NotificationHandler {

    private static Bitmap icon;;



    public static void sendNotification(Context ctx, String message) {

        final String TAG = ctx.getClass().getSimpleName();
        JSONObject jObject;
        Log.d(TAG, "Mesage: " + message);
        String json = message;
        try {
            jObject = new JSONObject(json);

            String msg = jObject.getString("message");

            String chatroomID = jObject.getString("chatroomID");

            //String mobileNo= jObject.getString("mobileNo");

            String destMobNo= jObject.getString("destMobNo");

            Log.d(TAG, "message : " + msg);
            if(MainApplication.isActiveChatroom(chatroomID)){

                Log.d(TAG, "ChatrromOpened .. No need of notification  " + message +" "+ chatroomID);
            }else {
                Log.d(TAG, "Chatrrom not open .. display notification  " + message+" "+ chatroomID);
                displyNotifivcation(ctx, msg, chatroomID, destMobNo);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }




    }

    public static void displyNotifivcation( Context ctx, String Msg,String  chId, String mobileNo){

        icon = BitmapFactory.decodeResource(ctx.getResources(),
                R.drawable.launcher_icon);

        Intent intent = new Intent(ctx, MainActivity.class);
        intent.putExtra("Message", Msg);
        intent.putExtra("chatroomID", chId);
        intent.putExtra("MobNo", mobileNo);


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(ctx)

                .setLargeIcon(icon)
                .setSmallIcon(R.drawable.launcher_icon)
                .setContentTitle("Indus")
                .setContentText(mobileNo+" : "+ Msg)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(Integer.parseInt(chId)/* ID of notification */, notificationBuilder.build());

    }

}
