package com.example.simulterra2.indus.Utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ranajit on 10-07-2016.
 */
public class Utils {


    /**
     * Checks if the mobile number entered is valid or not
     *
     * @param phoneNumber - Phone number to check for correctness
     * @return true if valid, else false
     */
    public static boolean isPhoneNumberValid(String phoneNumber) {

        return phoneNumber.length() > 9;
        // return isValid;
    }


    /**
     * TO check the email is valid or not
     *
     * @param email
     * @return
     */
    public static boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches()) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * Helper function to ease sending out Toast messages.
     *
     * @param ctx     - Context
     * @param message - Message to display as toast
     */
    public static void toastItShort(Context ctx, String message) {
        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
    }


    /**
     *
     */

    public static void showSnack(View v, String message) {
        Snackbar.make(v, message, Snackbar.LENGTH_SHORT).show();
    }


}
