package com.example.simulterra2.indus.Beans;

/**
 * Created by admin on 18-08-2016.
 */
public class Files {
    String filename="";
    String fileType="";
    String fileUri= "";

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
