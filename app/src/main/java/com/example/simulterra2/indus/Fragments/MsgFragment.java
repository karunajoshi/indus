package com.example.simulterra2.indus.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.simulterra2.indus.Activities.ChatActivity;
import com.example.simulterra2.indus.Adapters.ChatUsersAdapter;
import com.example.simulterra2.indus.Beans.ChatRoom;
import com.example.simulterra2.indus.Beans.MsgRooms;
import com.example.simulterra2.indus.Adapters.BaseSwipListAdapter;
import com.example.simulterra2.indus.Generics.Constants;
import com.example.simulterra2.indus.Generics.PrefManager;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.Utils.Utils;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ranajit on 7/7/2016.
 */

public class MsgFragment extends Fragment {
    public final String TAG = this.getClass().getSimpleName();
    private static final String TAB_POSITION = "tab_position";
    // private List<MsgRooms> mMsgList;
    private SwipeMenuListView mListView;
    private AppAdapter mAdapter;
    public TextView txtNoData;

    PrefManager prefManager;

    JSONArray chatRooms = null;
    List<ChatRoom> chatRoomArrayList = null;
    AVLoadingIndicatorView progresssDirectMsg;


    public MsgFragment() {

    }

    public static MsgFragment newInstance(int tabPosition) {
        MsgFragment fragment = new MsgFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_POSITION, tabPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        //  int tabPosition = args.getInt(TAB_POSITION);
        View v = inflater.inflate(R.layout.fragment_direct_msg, container, false);
        progresssDirectMsg = (AVLoadingIndicatorView) v.findViewById(R.id.progresssDirectMsg);

        prefManager = new PrefManager(getActivity());
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        txtNoData.setVisibility(View.GONE);
        chatRoomArrayList = new ArrayList<>();


        new getAllChatrrom(prefManager.getMobileNo()).execute();

        mListView = (SwipeMenuListView) v.findViewById(R.id.listView);


        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {


                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth(dp2px(90));

                openItem.setIcon(R.drawable.archive);
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.trash);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };


        // set creator
        mListView.setMenuCreator(creator);

        // step 2. listener item click event
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                ChatRoom item = chatRoomArrayList.get(position);
                switch (index) {
                    case 0:
                        // open
                        Log.d("", "delete clicked");
                        break;
                    case 1:

                        Log.d("", "Archive clicked");
                        break;
                }
                return false;
            }
        });

        // set SwipeListener
        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });

        // set MenuStateChangeListener
        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });


        // test item long click
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                Toast.makeText(getActivity(), position + " long click", Toast.LENGTH_SHORT).show();
                return false;
            }
        });


        return v;
    }


    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    class AppAdapter extends BaseSwipListAdapter {

        @Override
        public int getCount() {
            return chatRoomArrayList.size();
        }

        @Override
        public ChatRoom getItem(int position) {
            return chatRoomArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getActivity(),
                        R.layout.list_item_direct_msg, null);
                new ViewHolder(convertView);
            }
            final ViewHolder holder = (ViewHolder) convertView.getTag();
            final ChatRoom item = getItem(position);
            holder.txtName.setText(item.getMemberId());
            holder.txtMsg.setText(item.getLastMsg());

            holder.imgProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), ChatActivity.class).putExtra("ChatroomId", item.getChatRoomId()).putExtra("UserName", item.getMemberId()));
                }
            });
            holder.txtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(getActivity(), ChatActivity.class).putExtra("ChatroomId", item.getChatRoomId()).putExtra("UserName", item.getMemberId()));

                }
            });

            return convertView;
        }

        class ViewHolder {
            ImageView imgProfile;
            TextView txtName, txtTime, txtMsg;
            View mView;

            public ViewHolder(View view) {
                imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
                txtName = (TextView) view.findViewById(R.id.txtName);
                txtTime = (TextView) view.findViewById(R.id.txtTime);
                txtMsg = (TextView) view.findViewById(R.id.txtMsg);
                mView = view;
                view.setTag(this);
            }
        }

        @Override
        public boolean getSwipEnableByPosition(int position) {
            if (position % 2 == 0) {
                return false;
            }
            return true;
        }
    }


    /**
     * Register User here
     */
    public class getAllChatrrom extends AsyncTask<Void, Void, Void> {

        String mob;

        public getAllChatrrom(String mob) {
            this.mob = mob;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progresssDirectMsg.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("mobNo", mob);
                json = jsonObject.toString();
                Log.d("json", json);

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.GetAllChatRoomService);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);


                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                chatRooms = jObject.getJSONArray("chats");
                // looping through All Contacts
                for (int i = 0; i < chatRooms.length(); i++) {
                    JSONObject c = chatRooms.getJSONObject(i);

                    ChatRoom chatRoom = new ChatRoom();
                    chatRoom.setChatRoomId(c.getString("chat_room_id"));
                    chatRoom.setType(c.getString("chat_room_type"));
                    chatRoom.setLastMsg(c.getString("lastMsg"));
                    chatRoom.setMemberId(c.getString("otherMobileNo"));
                    Log.d(TAG, "Charoom Id " + c.getString("chat_room_id"));
                    chatRoomArrayList.add(chatRoom);
                }


            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {

            }


            progresssDirectMsg.setVisibility(View.GONE);
            if (chatRoomArrayList.size() > 0) {
                txtNoData.setVisibility(View.GONE);
                mAdapter = new AppAdapter();
                mListView.setAdapter(mAdapter);
            } else {
                txtNoData.setVisibility(View.VISIBLE);
                //  Utils.toastItShort(getActivity(), "No chatrooms present");
            }


        }
    }


}