package com.example.simulterra2.indus.Activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.simulterra2.indus.Generics.Constants;
import com.example.simulterra2.indus.Generics.NetworkDetector;
import com.example.simulterra2.indus.Generics.PrefManager;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.Utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.HttpURLConnection;

public class SplashActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();
    PrefManager prefManager;
    ImageView imgProfile;
    String exixt = "false";

    NetworkDetector cd;
    Boolean isInternetPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        cd = new NetworkDetector(SplashActivity.this);
        getSupportActionBar().hide();
        prefManager = new PrefManager(getApplicationContext());
        imgProfile = (ImageView) findViewById(R.id.imgLogo);
        animation2();


        isInternetPresent = cd.isNetworkAvailable();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, " json  User -> " + prefManager.getMobileNo());

                    if (prefManager.getMobileNo() == null) {

                        UserRegistrationActivity.navigate(SplashActivity.this, imgProfile, "Rnjt");
                        finish();
                    } else {

                        if (isInternetPresent) {
                            new CheckUserExist().execute();

                        } else {
                            finish();
                            //startActivity(new Intent(SplashActivity.this, MainActivity.class));
                            startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                        }
                    }
                } catch (Exception e) {

                }
            }
        }, 1500);
    }


    /**
     * splash top down animation
     */
    private void animation2() {
        imgProfile.setAlpha(1.0f);
        imgProfile.startAnimation(AnimationUtils.loadAnimation(this, R.anim.translate_top_to_center));
    }


    /**
     * CheckUserExist
     */
    public class CheckUserExist extends AsyncTask<Void, Void, Void> {

        public CheckUserExist() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("mobNo", prefManager.getMobileNo());
                json = jsonObject.toString();
                Log.d(TAG, " json  CheckUserExist -> " + json);

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.GetUserFromPhoneBookService);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);

               // Log.d(TAG, " json  CheckUserExist <- " + EntityUtils.toString(response.getEntity()));

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                String respCode = jObject.getString("exist");

                Log.d("respCode", respCode);
                setUserExist(respCode);


            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {

            }

            Log.d(TAG, " json  exixt -> " + exixt);
            if (exixt.equals("true")) {
                finish();
               // startActivity(new Intent(SplashActivity.this, MainActivity.class));
                startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
            } else {
                finish();

                UserRegistrationActivity.navigate(SplashActivity.this, imgProfile, "Rnjt");
                Utils.toastItShort(getApplicationContext(), "Something went wrong\nPlease Register again!");

            }

        }
    }


    /**
     * set existance
     *
     * @param exist
     */
    private void setUserExist(String exist) {
        this.exixt = exist;

    }


}


