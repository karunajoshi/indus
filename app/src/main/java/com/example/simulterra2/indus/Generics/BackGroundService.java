package com.example.simulterra2.indus.Generics;

import android.content.Intent;
import android.os.IBinder;
import android.app.Service;
import android.util.Log;

/**
 * Created by Ranajit on 17-07-2016.
 */
public class BackGroundService extends Service {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
        Log.d(TAG, "FirstService started");

        Intent i = new Intent("android.intent.action.MAIN");
        this.sendBroadcast(i);
        this.stopSelf();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d(TAG, "FirstService destroyed");
    }


}
