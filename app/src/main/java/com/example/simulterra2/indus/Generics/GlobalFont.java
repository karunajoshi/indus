package com.example.simulterra2.indus.Generics;


import android.app.ActionBar;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.EditTextPreference;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Ranajit  on 10-07-2016. to set custom font to Whole App
 */
public class GlobalFont extends Application {


    private static GlobalFont app;
    private static Context context;

    public void onCreate() {
        super.onCreate();
        GlobalFont.context = getApplicationContext();
        this.app = this;
    }

    public static GlobalFont getApp() {
        return app;
    }

    public static Context getStaticContext() {
        return GlobalFont.context;
    }

    public void setFont(Context contex, ViewGroup group, String fontName) {
        Typeface mFont = Typeface.createFromAsset(contex.getAssets(), "fonts/" + fontName);  //nexalight.otf
        int count = group.getChildCount();
        View v;
        for (int i = 0; i < count; i++) {
            v = group.getChildAt(i);
            if (v instanceof TextView || v instanceof Button || v instanceof EditText /*|| v instanceof TextInputLayout*/ /*etc.*/)
                ((TextView) v).setTypeface(mFont);

            else if (v instanceof TextInputLayout)
                setFont(contex, (ViewGroup) v, fontName);

            else if (v instanceof ViewGroup)
                setFont(contex, (ViewGroup) v, fontName);
        }
    }


}