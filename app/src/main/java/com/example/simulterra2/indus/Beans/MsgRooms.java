package com.example.simulterra2.indus.Beans;

/**
 * Created by simulterra2 on 7/7/2016.
 */
public class MsgRooms {

    private String uName;
    private String uMsg;
    private String uTime;

    public String getuMsg() {
        return uMsg;
    }

    public void setuMsg(String uMsg) {
        this.uMsg = uMsg;
    }

    public String getuTime() {
        return uTime;
    }

    public void setuTime(String uTime) {
        this.uTime = uTime;
    }



    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }
}
