package com.example.simulterra2.indus.Activities;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.support.multidex.MultiDex;
/**
 * Created by admin on 17-07-2016.
 */
public class MainApplication extends Application {

    private final static String TAG = MainApplication.class.getSimpleName();
    private static String ActiveChatRoomId = null;




    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Initializing App");



    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public static boolean isActiveChatroom(String ChtroomId) {

        if (ActiveChatRoomId != null) {
            if (!ActiveChatRoomId.equals(null)) {
                if (ActiveChatRoomId.equals(ChtroomId)) {

                    Log.e(TAG, "ActiveChatRoomId : " + ActiveChatRoomId + " msg received for ChatRoomId : " + ChtroomId);
                    return true;
                } else {
                    Log.e(TAG, " msg received for ChatRoomId : " + ChtroomId);
                    return false;
                }

            } else {
                Log.d(TAG, "ActiveChatRoomId : " + ActiveChatRoomId);
                return false;
            }

        } else {
            Log.d(TAG, "ActiveChatRoomId : " + ActiveChatRoomId);
            return false;
        }


    }

    public static void SetActiveChatroom(String ChtroomId) {
        ActiveChatRoomId = ChtroomId;
    }



}