package com.example.simulterra2.indus.Activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.simulterra2.indus.Generics.Constants;
import com.example.simulterra2.indus.Generics.GCM.GCMPreferences;
import com.example.simulterra2.indus.Generics.GCM.RegistrationIntentService;
import com.example.simulterra2.indus.Generics.NetworkDetector;
import com.example.simulterra2.indus.Generics.PrefManager;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.Utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class UserRegistrationActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    private static final String EXTRA_IMAGE = "IMG";
    private static final String EXTRA_TITLE = "TITLE";
    ImageView imgProfile;

    TextInputLayout edtMob, edtEmail, edtName;
    RelativeLayout layRelUserRegistrationActivity;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    PrefManager prefManager;

    NetworkDetector cd;
    Boolean isInternetPresent = false;
    public EditText edtMobs, edtEmails, edtNames;


    public static void navigate(AppCompatActivity activity, View transitionImage, String f) {
        Intent intent = new Intent(activity, UserRegistrationActivity.class);
        intent.putExtra(EXTRA_IMAGE, f);
        intent.putExtra(EXTRA_TITLE, f);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_IMAGE);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  initActivityTransitions();
        setContentView(R.layout.activity_user_registration);
        cd = new NetworkDetector(UserRegistrationActivity.this);

        prefManager = new PrefManager(getApplicationContext());

        ViewCompat.setTransitionName(findViewById(R.id.imgFullImage), EXTRA_IMAGE);
        imgProfile = (ImageView) findViewById(R.id.imgFullImage);
        imgProfile.setImageResource(R.drawable.logo);
        edtName = (TextInputLayout) findViewById(R.id.edtName);
        edtEmail = (TextInputLayout) findViewById(R.id.edtEmail);
        edtMob = (TextInputLayout) findViewById(R.id.edtMob);
        edtNames = (EditText) findViewById(R.id.edtNames);
        edtEmails = (EditText) findViewById(R.id.edtEmails);
        edtMobs = (EditText) findViewById(R.id.edtMobs);


        layRelUserRegistrationActivity = (RelativeLayout) findViewById(R.id.layRelUserRegistrationActivity);


        isInternetPresent = cd.isNetworkAvailable();

        if (isInternetPresent) {
            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    //  mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                    SharedPreferences sharedPreferences =
                            PreferenceManager.getDefaultSharedPreferences(context);
                    boolean sentToken = sharedPreferences
                            .getBoolean(GCMPreferences.SENT_TOKEN_TO_SERVER, false);
                    Log.d(TAG, "TOKEN sentToken : " + sentToken);

                    if (sentToken) {

                        String token = sharedPreferences.getString(GCMPreferences.REGISTRATION_TOKEN, null);
                        Log.d(TAG, "TOKEN : " + token);
                        prefManager.setDeviceToken(token);
                        // Utils.toastItShort(getApplicationContext(), "Token : " + token);
                    } else {
                        Utils.toastItShort(getApplicationContext(), "An error occurred while either fetching the InstanceID token,\n" +
                                "        sending the fetched token to the server or subscribing to the PubSub topic. Please try\n" +
                                "        running the sample again.");
                    }
                }
            };

            registerReceiver();

            if (checkPlayServices()) {
                // Start IntentService to register this application with GCM.
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            }


        } else {

            cd.showAlertDialog(UserRegistrationActivity.this, "No Internet Connection", "Please Check your network connection and then try again.", false);
        }
        getEmail();

    }


    /**
     * Do Login
     *
     * @param v onclick
     */
    public void doLogin(View v) {

        if (isInternetPresent) {
            String email = edtEmail.getEditText().getText().toString();
            String password = edtMob.getEditText().getText().toString();
            String name = edtName.getEditText().getText().toString();

            if (name.trim().length() == 0) {
                Utils.showSnack(layRelUserRegistrationActivity, "Not a valid Name!");

            } else if (!Utils.isEmailValid(email)) {
                //edtEmail.setError("Not a valid Email address!");
                Utils.showSnack(layRelUserRegistrationActivity, "Not a valid Email address!");

            } else if (!Utils.isPhoneNumberValid(password)) {

                Utils.showSnack(layRelUserRegistrationActivity, "Not a valid Mobile!");
                //   edtMob.setError("Not a valid Mobile!");
            } else {
                edtEmail.setErrorEnabled(false);
                edtMob.setErrorEnabled(false);
                String token = prefManager.getDeviceToken();
                Log.d(TAG, "TOKEN  : " + token);
                new RegisterUser(email, password, name.trim()).execute();
            }
        } else {

            cd.showAlertDialog(UserRegistrationActivity.this, "No Internet Connection", "Please Check your network connection and then try again.", false);
        }
    }


    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(GCMPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("REGISTRATION_COMPLETE"));

        registerReceiver();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    /**
     * Register User here
     */
    public class RegisterUser extends AsyncTask<Void, Void, Void> {

        String enmail;
        String mob;
        String name;

        public RegisterUser(String email, String mob, String name) {
            this.enmail = email;
            this.mob = mob;
            this.name = name;

        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("email", enmail);
                jsonObject.accumulate("mobNo", mob);
                jsonObject.accumulate("name", name);
                jsonObject.accumulate("gcmId", prefManager.getDeviceToken());
                json = jsonObject.toString();
                Log.d(TAG, " json  RegisterUser -> " + json);
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.CreateUserService);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);
                prefManager.setMobileNo(mob);
                // Log.d(TAG, " json  RegisterUser <- " + EntityUtils.toString(response.getEntity()));
                //Received response:  {"respCode":"00","respDesc":"User Created","userId":"7"}

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                String respCode = jObject.getString("respCode");
                String respDesc = jObject.getString("respDesc");
                String userId = jObject.getString("userId");

                Log.d("respCode", respCode);
                Log.d("respDesc", respDesc);
                Log.d("userId", userId);
                prefManager.setEmailID(enmail);
                prefManager.setUserID(userId);
                prefManager.setSampleString(name);


            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {
                finish();
                //startActivity(new Intent(UserRegistrationActivity.this, MainActivity.class));
                startActivity(new Intent(UserRegistrationActivity.this, DashBoardActivity.class));
            }
        }
    }

    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getEmail() {
        try {
            if (isInternetPresent) {

                ArrayList<String> Emaillist = new ArrayList<>();
                Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
                Account[] accounts = AccountManager.get(getApplicationContext()).getAccounts();
                for (Account account : accounts) {
                    if (emailPattern.matcher(account.name).matches()) {
                        String possibleEmail = account.name;
                        if (!Emaillist.contains(possibleEmail)) {
                            Emaillist.add(possibleEmail);
                        }
                        Log.d("getEmail()==>", "" + possibleEmail);
                        edtEmails.setText(possibleEmail);
                        // userEmail = possibleEmail;
                        //break;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
