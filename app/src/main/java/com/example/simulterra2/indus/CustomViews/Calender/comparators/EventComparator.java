package com.example.simulterra2.indus.CustomViews.Calender.comparators;


import com.example.simulterra2.indus.CustomViews.Calender.domain.EventModel;

import java.util.Comparator;

public class EventComparator implements Comparator<EventModel> {

    @Override
    public int compare(EventModel lhs, EventModel rhs) {
        return lhs.getTimeInMillis() < rhs.getTimeInMillis() ? -1 : lhs.getTimeInMillis() == rhs.getTimeInMillis() ? 0 : 1;
    }
}
