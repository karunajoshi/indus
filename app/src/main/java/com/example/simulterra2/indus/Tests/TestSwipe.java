package com.example.simulterra2.indus.Tests;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.simulterra2.indus.Beans.MsgRooms;
import com.example.simulterra2.indus.R;

import java.util.ArrayList;
import java.util.List;

public class TestSwipe extends Activity {

    private static final String TAB_POSITION = "tab_position";
    private List<MsgRooms> mMsgList;
    private SwipeMenuListView mListView;
    private AppAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_direct_msg);


        mMsgList = new ArrayList<MsgRooms>();
        MsgRooms rooms = new MsgRooms();

        rooms.setuMsg("Msg1");
        rooms.setuName("Rnjt");
        rooms.setuTime("10.15PM");

        for (int i = 0; i < 4; i++) {
            mMsgList.add(rooms);
        }


        // mAppList = getPackageManager().getInstalledApplications(0);
        mListView = (SwipeMenuListView) findViewById(R.id.listView);

        mAdapter = new AppAdapter();
        mListView.setAdapter(mAdapter);


        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth(dp2px(90));
                // set item title
                //  openItem.setTitle("Open");
                // set item title fontsize
                //penItem.setTitleSize(18);
                openItem.setIcon(R.drawable.trash);
                // set item title font color
              //  openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.archive);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };


        // set creator
        mListView.setMenuCreator(creator);

        // step 2. listener item click event
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                MsgRooms item = mMsgList.get(position);
                switch (index) {
                    case 0:
                        // open
                        Log.d("", "delete clicked");
                        break;
                    case 1:

                        Log.d("", "Archive clicked");
                        // delete
//					delete(item);
                        //  mAppList.remove(position);
                        //  mAdapter.notifyDataSetChanged();
                        break;
                }
                return false;
            }
        });

        // set SwipeListener
        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });

        // set MenuStateChangeListener
        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });

        // other setting
//		listView.setCloseInterpolator(new BounceInterpolator());

        // test item long click
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                Toast.makeText(getApplicationContext(), position + " long click", Toast.LENGTH_SHORT).show();
                return false;
            }
        });


    }


    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    class AppAdapter extends BaseSwipListAdapter {

        @Override
        public int getCount() {
            return mMsgList.size();
        }

        @Override
        public MsgRooms getItem(int position) {
            return mMsgList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getApplicationContext(),
                        R.layout.list_item_direct_msg, null);
                new ViewHolder(convertView);
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            MsgRooms item = getItem(position);
            // holder.imgProfile.setImageDrawable(item.loadIcon(getPackageManager()));
            holder.txtName.setText(item.getuMsg());
            holder.imgProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "imgProfile_click", Toast.LENGTH_SHORT).show();
                }
            });
            holder.txtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(getApplicationContext(), TestSwipe.class));
                    Toast.makeText(getApplicationContext(), "imgProfile_click", Toast.LENGTH_SHORT).show();
                }
            });
            return convertView;
        }

        class ViewHolder {
            ImageView imgProfile;
            TextView txtName;

            public ViewHolder(View view) {
                imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
                txtName = (TextView) view.findViewById(R.id.txtName);
                view.setTag(this);
            }
        }

        @Override
        public boolean getSwipEnableByPosition(int position) {
            if (position % 2 == 0) {
                return false;
            }
            return true;
        }
    }

    public abstract class BaseSwipListAdapter extends BaseAdapter {

        public boolean getSwipEnableByPosition(int position) {
            return true;
        }


    }
}
