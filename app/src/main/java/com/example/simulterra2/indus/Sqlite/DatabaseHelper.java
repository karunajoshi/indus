package com.example.simulterra2.indus.Sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Ranajit on 10-07-2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {


    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static DatabaseHelper sInstance = null;

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "indusDB";

    /**
     * Constructor should be private to prevent direct instantiation.
     * make call to static method "getInstance()" instead.
     *
     * @param context - Application Context
     */
    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Get instance of DatabaseHelper.
     *
     * @param context - App context
     */
    public static synchronized DatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }


    /**
     * Create the Schema
     *
     * @param db - SQLite DB instance
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        Log.d(TAG, "Creating tables");

    }

    /**
     * Upgrade the database schema
     *
     * @param db         - DB reference
     * @param oldVersion - Old version number
     * @param newVersion - New version number
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // To do stuff here...
    }


}
