package com.example.simulterra2.indus.Fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.simulterra2.indus.Adapters.ChatUsersAdapter;
import com.example.simulterra2.indus.Beans.ChatRoom;
import com.example.simulterra2.indus.Generics.Constants;
import com.example.simulterra2.indus.Generics.PrefManager;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.Utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by simulterra2 on 7/7/2016.
 */
public class GroupsFragment extends Fragment {

    public final String TAG = this.getClass().getSimpleName();
    private RecyclerView recyclerview;
    PrefManager prefManager;

    JSONArray chatRooms = null;
    ArrayList<HashMap<String, String>> chatRoomList;
    ArrayList<ChatRoom> chatRoomArrayList = null;
    ChatUsersAdapter rcAdapter;

    TextView txtNoData;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        //  int tabPosition = args.getInt(TAB_POSITION);
        View v = inflater.inflate(R.layout.fragment_groups, container, false);
        prefManager = new PrefManager(getActivity());


        chatRoomArrayList = new ArrayList<>();

        recyclerview = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerview.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        txtNoData.setVisibility(View.GONE);

        chatRoomList = new ArrayList<HashMap<String, String>>();

     //   new getAllChatrrom(prefManager.getMobileNo()).execute();


        return v;
    }


    /**
     * Register User here
     */
    public class getAllChatrrom extends AsyncTask<Void, Void, Void> {

        String mob;

        public getAllChatrrom(String mob) {
            this.mob = mob;
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("mobNo", mob);
                json = jsonObject.toString();
                Log.d("json", json);

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.GetAllChatRoomService);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);

                //Received response:  {"respCode":"00","respDesc":"User Created","userId":"7"}

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                chatRooms = jObject.getJSONArray("chats");
                // looping through All Contacts
                for (int i = 0; i < chatRooms.length(); i++) {
                    JSONObject c = chatRooms.getJSONObject(i);

                    ChatRoom chatRoom = new ChatRoom();
                    chatRoom.setChatRoomId(c.getString("chat_room_id"));
                    chatRoom.setType(c.getString("chat_room_type"));
                    chatRoom.setLastMsg(c.getString("lastMsg"));
                    //  chatRoom.setLastMsgReceivedOn(c.getString("lastMsgTime"));
                    chatRoom.setMemberId(c.getString("otherMobileNo"));
                    Log.d(TAG, "Charoom Id " + c.getString("chat_room_id"));
                    chatRoomArrayList.add(chatRoom);
                }


              /*  for (int i = 0; i < 10; i++) {


                    ChatRoom chatRoom = new ChatRoom();
                    chatRoom.setChatRoomId("id" + i);
                    chatRoom.setMemberId("mobile" + i);
                    chatRoom.setName("name" + i);
                    chatRoomArrayList.add(chatRoom);
                }*/
            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {

            }
            rcAdapter = new ChatUsersAdapter(getActivity(), chatRoomArrayList, new OnItemClickListener() {
                @Override
                public void onItemClick(ChatRoom item, View v, int pos) {
                    Log.d(TAG, "Clicked  Charoom Id " + item.getChatRoomId());
                }
            });

            if (chatRoomArrayList.size() > 0) {
                txtNoData.setVisibility(View.GONE);
                recyclerview.setAdapter(rcAdapter);
            } else {
                txtNoData.setVisibility(View.VISIBLE);
                Utils.toastItShort(getActivity(), "No chatrooms present");
            }


        }
    }


    public interface OnItemClickListener {
        void onItemClick(ChatRoom item, View v, int pos);
    }


}


