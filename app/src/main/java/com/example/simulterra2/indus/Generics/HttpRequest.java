package com.example.simulterra2.indus.Generics;


import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Ranajit on 10-07-2016.
 */


public class HttpRequest extends AsyncTask<ContentValues, Void, String> {

    private final String TAG = this.getClass().getSimpleName();

    private Callback.JSONCallback callback;
    private String url;
    private String type;

    /**
     * Constructor
     *
     * @param type     - POST or GET request
     * @param url      - URL of the server to issue the request against
     * @param callback - Callback function to be called when data has been received
     */
    public HttpRequest(String type, String url, Callback.JSONCallback callback) {
        //this.context = context;
        this.callback = callback;
        this.url = url;
        this.type = type;

    }

    /**
     * Background Task
     *
     * @param params - List of key-value pairs to be passed to the server
     * @return value retrieved from server
     */

    protected String doInBackground(ContentValues... params) {
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(this.url);//new URL(url);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.connect();

            //Create JSONObject here
            JSONObject jsonParam = new JSONObject();
            //for (String key : (ContentValues)pairs[0].keykeySet()) {
            for (ContentValues c : params) {
                for (String key : c.keySet()) {
                    jsonParam.put(key, c.get(key));
                }
            }
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonParam.toString());
            out.close();

            int HttpResult = urlConnection.getResponseCode();

            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                Log.d(TAG, "Received from server: " + sb.toString());

                return sb.toString();
            } else {
                Log.d(TAG, "Server Response: " + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return "";
    }

    /**
     * Post Execute call. Convert to JSON and pass it to the callback function.
     *
     * @param receivedValue - Values received from the server. This could be a JSON string that needs to be converted to a JSON object.
     */
    protected void onPostExecute(String receivedValue) {
        Log.d(TAG, "receivedValue = " + receivedValue);
        JSONObject json = null;
        try {
            json = new JSONObject(receivedValue);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        callback.call(json);
    }
}
