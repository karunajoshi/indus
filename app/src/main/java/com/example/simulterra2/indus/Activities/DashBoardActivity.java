package com.example.simulterra2.indus.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.simulterra2.indus.R;

public class DashBoardActivity extends AppCompatActivity {

    TextView txtChat, txtEmail, txtDrive, txtCalender, txtContact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        getSupportActionBar().hide();

        txtChat = (TextView) findViewById(R.id.txtChat);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtDrive = (TextView) findViewById(R.id.txtDrive);
        txtCalender = (TextView) findViewById(R.id.txtCalender);
        txtContact = (TextView) findViewById(R.id.txtContact);


    }

    /**
     * Open Contacts
     *
     * @param v
     */
    public void openContact(View v) {

        startActivity(new Intent(DashBoardActivity.this, Test.class));

    }


    /**
     * Open Chats
     *
     * @param v
     */
    public void openChat(View v) {

        startActivity(new Intent(DashBoardActivity.this, MainActivity.class));

    }

    /**
     * Open Emails
     *
     * @param v
     */
    public void openEmail(View v) {

        startActivity(new Intent(DashBoardActivity.this, EmailActivity.class));

    }

    /**
     * Open Drive
     *
     * @param v
     */
    public void openDrive(View v) {

        startActivity(new Intent(DashBoardActivity.this, DriveActivity.class));

    }

    /**
     * Open Calender
     *
     * @param v
     */
    public void openCalender(View v) {

        startActivity(new Intent(DashBoardActivity.this, CalenderActivity.class));

    }


}
