package com.example.simulterra2.indus.Generics;

import org.json.JSONObject;

/**
 * Created by Ranajit on 10-07-2016.
 */
public class Callback {

    public interface JSONCallback {

        void call(JSONObject JSON);
    }
}
