package com.example.simulterra2.indus.Beans;

import com.example.simulterra2.indus.Generics.Constants;

import java.util.Date;

/**
 * Created by Ranajit on 14-07-2016.
 */
public class ChatRoom {
    private String  chatRoomId = "";
    private String  memberId   = "";
    private String  type       = Constants.TYPE_SINGLE;
    private String  name       = "";
    private String  phone      = "";



    private String  lastMsg      = "";
    private long    lastMsgReceivedOn     = 0;
    private boolean hasNewMsg  = true;
    private long    createdOn = new Date().getTime();

    public ChatRoom(){ }

    public String getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(String chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLastMsgReceivedOn() {
        return lastMsgReceivedOn;
    }

    public void setLastMsgReceivedOn(long lastMsgReceivedOn) {
        this.lastMsgReceivedOn = lastMsgReceivedOn;
    }

    public boolean hasNewMsg() {
        return hasNewMsg;
    }

    public void setHasNewMsg(boolean hasNewMsg) {
        this.hasNewMsg = hasNewMsg;
    }

    public String getPhone() {  return phone; }

    public void setPhone(String phone) { this.phone = phone; }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }
    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }
}
