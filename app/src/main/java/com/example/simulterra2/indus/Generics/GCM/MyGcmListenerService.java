package com.example.simulterra2.indus.Generics.GCM;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.simulterra2.indus.Activities.MainActivity;
import com.example.simulterra2.indus.Generics.NotificationHandler;
import com.example.simulterra2.indus.R;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ranajit on 10-07-2016.
 */
public class MyGcmListenerService extends GcmListenerService {
    private static Bitmap icon;
    private final String TAG = this.getClass().getSimpleName();
    Bundle d;

    /**
     * Called when message is received.
     *
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        this.d = data;
       // Bundle[{chatRoomId=29, key=GCM_CHAT, title=gdg, message=gdg, collapse_key=do_not_collapse}]

     //   Bundle[{chatRoomId=29, srcMobNo=8879917028, key=GCM_CHAT, title=hiduu, destUserId=39, message=hiduu, collapse_key=do_not_collapse, destMobNo=9870317506}]
       // String message = data.getString("data");
        String chatroomID = data.getString("chatRoomId");
        String message = data.getString("message");
        String destMobNo = data.getString("srcMobNo");

        // String message = data.getString("data");

        icon = BitmapFactory.decodeResource(getApplication().getResources(),
                R.drawable.logo);
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        Log.d(TAG, "destMobNo: " + destMobNo);


        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        // sendNotification(message);
       // sendNotification(message);
        JSONObject j = new JSONObject();
        try {

            j.accumulate("message", message);
            j.accumulate("chatroomID", chatroomID);
            j.accumulate("destMobNo", destMobNo);

             NotificationHandler.sendNotification(this, ""+ j);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        Log.d(TAG, "Mesage: " + message);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)

                .setLargeIcon(icon)
                .setSmallIcon(R.drawable.launcher_icon)
                .setContentTitle("INDUS")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
