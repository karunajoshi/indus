package com.example.simulterra2.indus.Activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simulterra2.indus.Adapters.ChatUsersAdapter;
import com.example.simulterra2.indus.Beans.ChatRoom;
import com.example.simulterra2.indus.Beans.Email;
import com.example.simulterra2.indus.Fragments.GroupsFragment;
import com.example.simulterra2.indus.Generics.Constants;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.Utils.Utils;
import com.example.simulterra2.indus.ViewHolders.ChatRoomViewHolder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EmailActivity extends AppCompatActivity {
    public final String TAG = this.getClass().getSimpleName();
    JSONArray chatRooms = null;
    //  ArrayList<HashMap<String, String>> chatRoomList;
    ArrayList<Email> EmailList = null;
    EmailAdapter rcAdapter;
    private RecyclerView recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        getSupportActionBar().setTitle("Email");



        EmailList= new ArrayList<>();

        recyclerview = (RecyclerView) findViewById(R.id.recyclerViewEmail);
        recyclerview.setLayoutManager(new GridLayoutManager(EmailActivity.this, 1));
        // EmailList = new ArrayList<HashMap<String, String>>();

        new getAllEmails("1").execute();

    }


    public class EmailAdapter extends RecyclerView.Adapter<ChatRoomViewHolder> {


        private List<Email> itemList;
        private Context context;
        HashMap<String, ChatRoom> mSelectedProspects = new HashMap<>();
        ChatRoom p;
        //  public final GroupsFragment.OnItemClickListener listener;


        public EmailAdapter(Context context, List<Email> itemList) {
            this.itemList = itemList;
            this.context = context;
            //  this.listener = listener;
        }

        @Override
        public ChatRoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_email, null);//list_item_responded_users
            ChatRoomViewHolder rcv = new ChatRoomViewHolder(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(ChatRoomViewHolder holder, int position) {

            //  holder.bind(itemList.get(position), listener);
            Log.d("", "itemList " + itemList.get(position).getBody());
            holder.txtName.setText(itemList.get(position).getFrom());
            holder.txtMsg.setText(itemList.get(position).getBody());
            holder.txtSub.setText(itemList.get(position).getSubj());


        }

        @Override
        public int getItemCount() {
            return this.itemList.size();
        }


    }

    /**
     * Register User here
     */
    public class getAllEmails extends AsyncTask<Void, Void, Void> {

        String mob;

        public getAllEmails(String mob) {
            this.mob = mob;
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("mobNo", mob);
                json = jsonObject.toString();
                Log.d("json", json);

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.GetEmailListService);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);

                //Received response:  {"respCode":"00","respDesc":"User Created","userId":"7"}

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                chatRooms = jObject.getJSONArray("emails");
                // looping through All Contacts
                for (int i = 0; i < chatRooms.length(); i++) {
                    JSONObject c = chatRooms.getJSONObject(i);

                    Email email = new Email();
                    email.setFrom(c.getString("from"));
                    email.setBody(c.getString("body"));
                    email.setSubj(c.getString("subj"));
                    //  chatRoom.setLastMsgReceivedOn(c.getString("lastMsgTime"));
                    // email.setMemberId(c.getString("otherMobileNo"));
                    // Log.d(TAG, "Charoom Id " + c.getString("chat_room_id"));
                    EmailList.add(email);
                }


              /*  for (int i = 0; i < 10; i++) {


                    ChatRoom chatRoom = new ChatRoom();
                    chatRoom.setChatRoomId("id" + i);
                    chatRoom.setMemberId("mobile" + i);
                    chatRoom.setName("name" + i);
                    chatRoomArrayList.add(chatRoom);
                }*/
            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {

            }
            rcAdapter = new EmailAdapter(getApplicationContext(), EmailList);

            if (EmailList.size() > 0) {
               // txtNoData.setVisibility(View.GONE);
                recyclerview.setAdapter(rcAdapter);
            } else {
                // txtNoData.setVisibility(View.VISIBLE);
                Utils.toastItShort(getApplicationContext(), "No Emails Found");
            }


        }
    }
}
