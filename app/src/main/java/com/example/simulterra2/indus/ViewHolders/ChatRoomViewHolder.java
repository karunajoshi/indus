package com.example.simulterra2.indus.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.simulterra2.indus.Beans.ChatRoom;
import com.example.simulterra2.indus.Fragments.GroupsFragment;
import com.example.simulterra2.indus.R;

/**
 * Created by Ranajit on 14-07-2016.
 */
public class ChatRoomViewHolder extends RecyclerView.ViewHolder {
    public ImageView imgProfile;
    public TextView txtName, txtTime, txtMsg, txtSub;
    public View mView;
    public ImageView imgFileType;
    public TextView txtFileName;

    public ChatRoomViewHolder(View view) {
        super(view);
        imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
        txtName = (TextView) view.findViewById(R.id.txtName);
        txtTime = (TextView) view.findViewById(R.id.txtTime);
        txtMsg = (TextView) view.findViewById(R.id.txtMsg);
        txtSub = (TextView) view.findViewById(R.id.txtSubject);

        imgFileType = (ImageView) view.findViewById(R.id.imgFileType);
        txtFileName = (TextView) view.findViewById(R.id.txtFileName);

        mView = view;
        view.setTag(this);
    }

    public void bind(final ChatRoom item, final GroupsFragment.OnItemClickListener listener) {

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item, v, getPosition());
            }
        });
    }
}