package com.example.simulterra2.indus.Fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.simulterra2.indus.Generics.Constants;
import com.example.simulterra2.indus.Generics.PrefManager;
import com.example.simulterra2.indus.R;
import com.example.simulterra2.indus.Utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.HttpURLConnection;

//import android.support.v4.app.Fragment;

/**
 * Created by simulterra2 on 7/7/2016.
 */
public class StarredFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();
    PrefManager prefManager;
    EditText edtMobs, edtMsg;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        //  int tabPosition = args.getInt(TAB_POSITION);
        View v = inflater.inflate(R.layout.fragment_starred, container, false);
        prefManager = new PrefManager(getActivity());


        edtMobs = (EditText) v.findViewById(R.id.edtMobs);
        edtMsg = (EditText) v.findViewById(R.id.edtMsgs);
        Button AddChatroom = (Button) v.findViewById(R.id.AddChatroom);

        AddChatroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtMobs.getText().toString().trim().length() > 5 && edtMsg.getText().toString().trim().length() > 0) {
                      new AddMessage(edtMobs.getText().toString().trim(), edtMsg.getText().toString().trim()).execute();

                }


            }
        });
        return v;
    }




    public class AddMessage extends AsyncTask<Void, Void, Void> {

        String mob;
        String msg;


        public AddMessage(String mob, String msg) {
            this.mob = mob;
            this.msg = msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("srcMobNo", prefManager.getMobileNo());
                jsonObject.accumulate("destMobNo", mob);
                jsonObject.accumulate("msg", msg);
                json = jsonObject.toString();
                Log.d(TAG, " json  AddMessage -> " + json);
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.AddMessageService);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);
              //  Log.d(TAG, " json  AddMessage <- " + EntityUtils.toString(response.getEntity()));

                //Received response:  {"respCode":"00","respDesc":"User Created","userId":"7"}

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                String respDesc = jObject.getString("respDesc");

                Log.d("respDesc", respDesc);

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        /* nothing to do here */
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {
                // finish();
                // startActivity(new Intent(getActivity(), MainActivity.class));

                Utils.toastItShort(getActivity(), "Message Sent to : " + edtMobs.getText().toString().trim());
                edtMobs.setText("");
                edtMsg.setText("");
            }
        }
    }




    /**
     * Register User here
     */
/*    public class AddChatrrom extends AsyncTask<Void, Void, Void> {

        String mob;

        public AddChatrrom(String mob) {
            this.mob = mob;
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            String json = null;
            JSONObject jObject = null;


            try {
                HttpResponse response;
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("chatRoomName", "Room1");
                jsonObject.accumulate("chatRoomParticipants", "" + prefManager.getMobileNo() + "," + mob);
                json = jsonObject.toString();
                Log.d("json", json);
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.CreateChatRoomService);
                httpPost.setEntity(new StringEntity(json, "UTF-8"));
                response = httpClient.execute(httpPost);

                //Received response:  {"respCode":"00","respDesc":"User Created","userId":"7"}

                jObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                String respCode = jObject.getString("respCode");
                String chatRoomId = jObject.getString("chatRoomId");
                String respDesc = jObject.getString("respDesc");

                Log.d("respCode", respCode);
                Log.d("respDesc", respDesc);
                Log.d("userId", chatRoomId);

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());

            } finally {
        *//* nothing to do here *//*
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
            } else {
                // finish();
                // startActivity(new Intent(getActivity(), MainActivity.class));

                Utils.toastItShort(getActivity(), "User created : " + edtMobs.getText().toString().trim());
                edtMobs.setText("");
            }
        }
    }*/
}
