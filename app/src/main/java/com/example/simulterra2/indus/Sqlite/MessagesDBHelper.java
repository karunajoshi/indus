package com.example.simulterra2.indus.Sqlite;

import android.provider.BaseColumns;

/**
 * Created by Ranajit on 10-07-2016.
 */
public class MessagesDBHelper implements BaseColumns {

    public static final String TABLE_MESSAGES = "messages";

    // Column Names
    public static final String KEY_FROM = "from";
    public static final String KEY_MSG = "msg";
    public static final String KEY_RECEIVED_TIMESTAMP = "received_timestamp";
    public static final String KEY_SENT_TIMESTAMP = "sent_timestamp";
    public static final String KEY_DELETED = "deleted";

    // MESSAGES table create statement
    public static final String CREATE_TABLE_MESSAGES = "CREATE TABLE " + TABLE_MESSAGES
            + "("
            + KEY_FROM + " TEXT,"
            + KEY_MSG + " TEXT,"
            + KEY_RECEIVED_TIMESTAMP + " INTEGER,"
            + KEY_SENT_TIMESTAMP + " INTEGER,"
            + ")";

}